#language: es
Característica: Servir café
  El café no se debe servir hasta que se pague
  El café no se debe servir hasta que se haya pulsado el botón
  Si no queda café entonces el dinero debe ser devuelto

Escenario: Comprar último café
  Dado que hay 1 café que quedan en la máquina
  Y he depositado 1 $
  Cuando presiono el botón de café
  Entonces 1 café debería ser servido